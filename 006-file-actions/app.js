import * as fs from 'fs';

const date = new Date().toISOString().substring(0, 10);
const time = new Date().toTimeString().substring(0, 8);
const file = 'dateAndTime.txt';
const fileContent = Bun.file(file);

Bun.write(fileContent, date + ' ' + time);

fs.appendFile(file, `\n${date} ${time}`, err => {
  if (err) console.log(err);
});

console.log(await fileContent.text());
