Bun.serve({
  fetch(req) {
    const url = new URL(req.url);
    if (url.pathname === '/') return new Response(`Home page!`);
    else if (url.pathname === '/blog') return new Response('Blog!');
    else return new Response(`404!`);
  },
  port: 3000,
});
