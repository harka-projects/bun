const server = Bun.serve({
  fetch() {
    return new Response('Bun!');
  },
});

setTimeout(() => {
  server.stop();
}, 5000);
