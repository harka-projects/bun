Bun.serve({
  fetch(req) {
    throw new Error('Whoops!');
  },
  error(error) {
    return new Response(`<pre>${error}\n${error.stack}</pre>`, {
      headers: {
        'Content-Type': 'text/html',
      },
    });
  },
});
