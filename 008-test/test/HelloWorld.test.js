import { expect, test } from 'bun:test'
import HelloWorld from '../src/HelloWorld'

test('Hello World should return "Oh oh..."', () => {
  expect(HelloWorld()).toBe('Oh oh...')
})

test('Hello World should return "Hello World!"', () => {
  expect(HelloWorld()).toBe('Hello World!')
})

test.todo('Hello World should return "Hello World!!!"', () => {})
